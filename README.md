# BackedBy Profile Wrapper

This repo contains contracts for wrapping BackedBy profiles as OpenGSN-compatible ERC721s. There is not much to them, so taking a quick look should be plenty to understand how they work (if already familiar with the BB contracts).

The use of minimal proxy makes the wrapping of contracts, and cloning of `MinProfile`, relatively cheap.

I have not implemented any metadata stuff for the NFT. Obtaining the profile data would require an HTML NFT using `animation_url` where the page pulls the CID from an IPFS gateway. Otherwise, we only know the profile ID and other on-chain data.

## Brief overview

`MinProfileFactory.sol` is the parent contract responsible for deploying new wrapped profiles. It keeps track of the true owner of a profile, the address of the profile's proxy contract, and other variable information relating to a given wrapped profile.

`MinProfile.sol` is the contract that implements the functionality of a BackedBy profile and is assigned as the owner. It refers to the factory contract to determine its owner and the profile ID it is associated with.

`ProfilePaymaster.sol` is the contract responsible for paying the gas of gasless transactions. It is only an example contract. It implements a basic whitelist so only permitted addresses can get gasless tx. It also ensures only transactions to the factory or a proxy are accepted.

## Minimal proxy

To save gas on deploying new wrapped profile contracts, [minimal proxy](https://eips.ethereum.org/EIPS/eip-1167) is used.

When `MinProfileFactory` is deployed, a single instance of `MinProfile` is deployed as well. When any future profile is created, this original instance of `MinProfile` is cloned via minimal proxy.

Importantly, this means that all subsequent profiles execute the code of `MinProfile.sol` in their own context. **Storage of the clones and the original must remain the same for this to be safe**.

To simplify this, I have tried to avoid any changing state in `MinProfile` at all. Instead, it calls back to the factory contract.

Please alert me if anything I have done is not secure - this is my first time using a minimal proxy!

## Paymaster

For these contracts to be compatible with OpenGSN, they must be deployed with a trusted forwarder address. Furthermore, a paymaster must be set up to pay for the gas of any gasless transactions.

The paymaster would determine who can/cannot utilise gasless payments.

## Testing

I have not written/done any meaningful tests of these contracts. I have compiled them, deployed them, created a test profile, and made a test post, but that is about it. I intend to test them thoroughly when time permits. Last time I tried to test them, the OpenGSN relayer was down :(

## Benefits

These contracts are bare-bones, and lots of possible features have not yet been implemented.

These contracts are **fully gasless**, meaning that all actions a profile owner may wish to perform can be achieved without paying for gas. That includes profile creation, profile editing, post creation and post editing.

Wrapped profiles are transferrable NFTs, so the ownership of a profile can be moved easily from one user to another.

A wrapped profile is owned by a `MinProfile` clone. This enables the `IBBPermissionsV01` hook `canViewSubscription` when locked content is viewed. If an `accessOverride` is set for a profile, custom logic can be used to permit access to a profile's content.

## Limitations

Wrapping existing profiles is possible, but it requires a manual transfer of ownership from the current owner via an edit of the profile. It is not possible to make this gasless as `BBProfiles` is not set up to support meta transactions.

As such, creating a wrapped profile from an existing profile is a two step process: mint the NFT and create the min proxy owner, then have the user manually transfer ownership to the proxy address. This sucks, but it is the best I could do.

This process also creates the possibility of someone minting an NFT for a profile and never making the proxy the owner. I have not properly accounted for this possibility in the current version of the contracts.

Contract-owned profiles are not currently supported by the official backed.by frontend. An alternative is required. Additionally, unwrapping a profile and returning it to the true owner does not make the profile compatible.

Currently, `accessOverride` is limited to permit access to an entire profile's content or none of it at all. A possible solution for this is described in BBDP-1.
