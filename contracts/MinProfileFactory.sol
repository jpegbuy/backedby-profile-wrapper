// SPDX-License-Identifier: MIT
pragma solidity ^0.8.17;

import "./MinProfile.sol";

import "@opengsn/contracts/src/ERC2771Recipient.sol";

import "@openzeppelin/contracts/utils/Strings.sol";
import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/token/ERC721/ERC721.sol";
import "@openzeppelin/contracts/token/ERC721/extensions/ERC721Burnable.sol";
import "@openzeppelin/contracts/proxy/Clones.sol";

import "./helpers/ProfileSetup.sol";
import "@backedby/v1-contracts/contracts/interfaces/IBBPermissionsV01.sol";
import "@backedby/v1-contracts/contracts/interfaces/IBBProfiles.sol";

// NFT wrapped BackedBy profiles.
// Can create a new profile and wrap it or wrap an existing profile by transferring ownership.

contract MinProfileFactory is ERC2771Recipient, Ownable, ERC721, ERC721Burnable {
    using Strings for uint256;

    MinProfile cloneSource;
    IBBProfiles profiles = IBBProfiles(0x096741579bAC68b4044Bbb4966D390E51081c7dC);
    ProfileSetup profileSetup = ProfileSetup(0x73f77e84188B8acf9a5b463a483600ED9C0DC9CE);

    mapping(address => bool) public proxyExists;
    mapping(uint256 => address) public idToProxy;
    mapping(address => uint256) public proxyToId;
    mapping(address => address) public proxyToOverride;

    constructor(address _forwarder) ERC721("BackedBy Profile", "BBP") {
        cloneSource = new MinProfile(address(this), address(_forwarder));
        _setTrustedForwarder(_forwarder);
    }

    function ownerOfContract(address _proxy) public view returns(address) {
        return ownerOf(proxyToId[_proxy]);
    }

    function idOfContract(address _proxy) public view returns(uint256) {
        return proxyToId[_proxy];
    }

    function overrideOfContract(address _proxy) public view returns(address) {
        return proxyToOverride[_proxy];
    }

    function isProxy(address _addr) public view returns (bool) {
        return proxyExists[_addr];
    }

    function mintNew(
        string memory profileCid, 
        uint256[] memory tierPrices, 
        string[] memory tierCids, 
        bool[] memory tierDeprecations, 
        address[] memory tierCurrencies, 
        uint256[] memory tierMultipliers, 
        uint256 subscriptionContribution
    ) external returns (address) {
        uint256 id = profiles.totalProfiles();
        address newProxy = Clones.clone(address(cloneSource));

        proxyExists[newProxy] = true;
        idToProxy[id] = newProxy;
        proxyToId[newProxy] = id;
        _safeMint(_msgSender(), id);

        profileSetup.setup(
            newProxy,
            _msgSender(),
            profileCid,
            tierPrices,
            tierCids,
            tierDeprecations,
            tierCurrencies,
            tierMultipliers,
            subscriptionContribution
        );

        return(newProxy);
    }

    function mintExisting(
        uint256 profileId
    ) external returns (address) {
        (address expectedAddr, ,) = profiles.getProfile(profileId);
        require(_msgSender() == expectedAddr, "Not owner");

        address newProxy = Clones.clone(address(cloneSource));
        proxyExists[newProxy] = true;
        idToProxy[profileId] = newProxy;
        proxyToId[newProxy] = profileId;

        _safeMint(_msgSender(), profileId);
        return(newProxy);
    }

    function setOverride(uint256 tokenId, address _override) external {
        require(_msgSender() == ownerOf(tokenId), "You can only set the override via the proxy");
        proxyToOverride[idToProxy[tokenId]] = _override;
    }
    
    function burn(uint256 tokenId) public override {
        require(_msgSender() == idToProxy[tokenId], "You can only burn via the proxy");
        proxyExists[idToProxy[tokenId]] = false;
        idToProxy[tokenId] = address(0);
        _burn(tokenId);
    }

    function _msgSender()
        internal
        view
        override(Context, ERC2771Recipient)
        returns (address sender)
    {
        sender = ERC2771Recipient._msgSender();
    }

    function _msgData()
        internal
        view
        override(Context, ERC2771Recipient)
        returns (bytes calldata)
    {
        return ERC2771Recipient._msgData();
    }

}
