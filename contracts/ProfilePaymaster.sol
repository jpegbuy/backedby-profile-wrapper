// SPDX-License-Identifier: MIT
pragma solidity ^0.8.17;

import "@opengsn/contracts/src/BasePaymaster.sol";
import "./MinProfileFactory.sol";

contract ProfilePaymaster is BasePaymaster {

    MinProfileFactory factory;
    bool public useSenderWhitelist = true;
    bool public useRejectOnRecipientRevert = false;
    mapping(address => bool) public senderWhitelist;

    constructor(address _factory) {
        factory = MinProfileFactory(_factory);
        senderWhitelist[msg.sender] = true;
    }

    function versionPaymaster() external view override virtual returns (string memory){
        return "3.0.0-beta.3";
    }

    function whitelistSender(address sender, bool isAllowed) public onlyOwner {
        senderWhitelist[sender] = isAllowed;
    }

    function setFactory(address _factory) public onlyOwner {
        factory = MinProfileFactory(_factory);
    }

    function setConfiguration(
        bool _useSenderWhitelist,
        bool _useRejectOnRecipientRevert
    )
        public
        onlyOwner
    {
        useSenderWhitelist = _useSenderWhitelist;
        useRejectOnRecipientRevert = _useRejectOnRecipientRevert;
    }

    function _preRelayedCall(
        GsnTypes.RelayRequest calldata relayRequest,
        bytes calldata signature,
        bytes calldata approvalData,
        uint256 maxPossibleGas
    )
        internal
        override
        virtual
        returns (bytes memory context, bool revertOnRecipientRevert)
    {
        (signature, maxPossibleGas);
        require(approvalData.length == 0, "approvalData: invalid length");
        require(relayRequest.relayData.paymasterData.length == 0, "paymasterData: invalid length");

        address sender = relayRequest.request.from;

        if (useSenderWhitelist) {
            require(senderWhitelist[sender], "not whitelisted");
        }

        address target = relayRequest.request.to;
        bool isFactory = target == address(factory);
        require(isFactory || factory.isProxy(target), "not a proxy or factory");
        
        if (!isFactory) {
            require(factory.ownerOfContract(target) == sender, "not owner");
        }

        return ("", useRejectOnRecipientRevert);
    }

    function _postRelayedCall(
        bytes calldata context,
        bool success,
        uint256 gasUseWithoutPost,
        GsnTypes.RelayData calldata relayData
    )
        internal
        override
        virtual
    {
        (context, success, gasUseWithoutPost, relayData);
    }
}