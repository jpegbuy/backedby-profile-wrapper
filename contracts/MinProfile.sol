// SPDX-License-Identifier: MIT
pragma solidity ^0.8.17;

import "./MinProfileFactory.sol";

import "@opengsn/contracts/src/ERC2771Recipient.sol";
import "@backedby/v1-contracts/contracts/interfaces/IBBSubscriptionsFactory.sol";
import "@backedby/v1-contracts/contracts/interfaces/IBBPermissionsV01.sol";
import "@backedby/v1-contracts/contracts/interfaces/IBBProfiles.sol";
import "@backedby/v1-contracts/contracts/interfaces/IBBTiers.sol";
import "@backedby/v1-contracts/contracts/interfaces/IBBPosts.sol";

// This contract is cloned via minimal proxy and owns a BBProfile.
// It wraps BBProfile functions and defers to the factory for profileId/owner.

contract MinProfile is ERC2771Recipient, IBBPermissionsV01 {

    IBBSubscriptionsFactory subFactory = IBBSubscriptionsFactory(0x5D2A904E7374cc3FaA5658Ecd462e370aA4637a6);
    IBBProfiles profiles = IBBProfiles(0x096741579bAC68b4044Bbb4966D390E51081c7dC);
    IBBTiers tiers = IBBTiers(0xfFec1c5B14808D56a894916A52da300d8eE77941);
    IBBPosts posts = IBBPosts(0x35bb26163E8Ec8542863D270fd92927F562Af1C6);

    MinProfileFactory factory;

    constructor(address _factory, address _trustedForwarder) {
        factory = MinProfileFactory(_factory);
        _setTrustedForwarder(_trustedForwarder);
    }

    function owner() public view returns (address) {
        return factory.ownerOfContract(address(this));
    }

    function id() public view returns (uint256) {
        return factory.idOfContract(address(this));
    }

    function accessOverride() public view returns (address) {
        return factory.overrideOfContract(address(this));
    }

    function editProfile(address receiver, string calldata cid) external {
        require(_msgSender() == owner(), "not owner");
        profiles.editProfile(id(), address(this), receiver, cid);
    }

    function createTiers(
        uint256[] calldata prices,
        string[] calldata cids,
        bool[] memory deprecated,
        address[] calldata supportedCurrencies,
        uint256[] calldata priceMultipliers
    ) external returns (uint256 tierSetId) {
        require(_msgSender() == owner(), "not owner");
        return
            tiers.createTiers(
                id(),
                prices,
                cids,
                deprecated,
                supportedCurrencies,
                priceMultipliers
            );
    }

    function editTiers(
        uint256 tierSetId,
        uint256[] calldata prices,
        string[] calldata cids,
        bool[] memory deprecated
    ) external {
        require(_msgSender() == owner(), "not owner");
        tiers.editTiers(id(), tierSetId, prices, cids, deprecated);
    }

    function setSupportedCurrencies(
        uint256 tierSetId,
        address[] calldata supportedCurrencies,
        uint256[] calldata priceMultipliers
    ) external {
        require(_msgSender() == owner(), "not owner");
        tiers.setSupportedCurrencies(
            id(),
            tierSetId,
            supportedCurrencies,
            priceMultipliers
        );
    }

    function createSubscriptionProfile(
        uint256 tierSetId,
        uint256 contribution
    ) external {
        require(_msgSender() == owner(), "not owner");
        subFactory.createSubscriptionProfile(
            id(),
            tierSetId,
            contribution
        );
    }

    function setContribution(uint256 contribution) external {
        require(_msgSender() == owner(), "not owner");
        subFactory.setContribution(id(), contribution);
    }

    function createPost(string calldata cid) external returns (uint256 postId) {
        require(_msgSender() == owner(), "not owner");
        return posts.createPost(id(), cid);
    }

    function editPost(uint256 postId, string calldata cid) external {
        require(_msgSender() == owner(), "not owner");
        posts.editPost(id(), postId, cid);
    }

    function unwrap() external {
        require(_msgSender() == owner(), "not owner");
        uint256 profileId = id();
        (, address receiver, string memory cid) = profiles.getProfile(profileId);
        profiles.editProfile(profileId, owner(), receiver, cid);
        factory.burn(profileId);
    }

    function canViewSubscription(address account) external view returns (bool) {
        if (owner() == account) return true;
        address ao = accessOverride();

        if (ao.code.length > 0) {
            try
                IBBPermissionsV01(ao).canViewSubscription(account)
            returns (bool success) {
                if (success) return success;
            } catch {}
        }

        return false;
    }

}
